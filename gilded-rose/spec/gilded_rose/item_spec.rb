require_relative '../spec_helper'

RSpec.describe GildedRose::Item do
  it_behaves_like 'item'

  subject { described_class.new(item) }

  let(:item)      { Item.new('name', sell_in, quality) }
  let(:sell_in)   { rand(2..10) }
  let(:quality)   { rand(4..50) }

  specify do
    expect {
      subject.age
    }.to change(subject, :quality).by(0)
  end

end

