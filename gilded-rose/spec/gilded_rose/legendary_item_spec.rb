require_relative '../spec_helper'

RSpec.describe GildedRose::LegendaryItem do

  subject { described_class.new(item) }

  let(:item)      { Item.new('name', sell_in, quality) }
  let(:sell_in)   { rand(-50..50) }
  let(:quality)   { rand(-50..50) }

  specify do
    expect {
      subject.age
    }.to change(subject, :quality).by(0)
  end

  specify do
    expect {
      subject.age
    }.to change(subject, :sell_in).by(0)
  end

end

