require_relative '../spec_helper'

RSpec.describe GildedRose::LimitedItem do

  it_should_behave_like 'item'
  it_should_behave_like 'appreciating_quality'

  subject { described_class.new(item) }

  let(:item)    { Item.new('name', sell_in, quality) }
  let(:quality) { rand(1..49) }
  let(:sell_in) { 12 }

  specify do
    expect {
      subject.age
    }.to change(subject, :quality).by(1)
  end

  context 'sell_in is between 6 and 10' do

    let(:sell_in) { rand(7..11) }
    specify do
      expect {
        subject.age
      }.to change(subject, :quality).by(2)
    end
  end

  context 'sell_in is between 1 and 5' do

    let(:sell_in) { rand(2..6) }

    specify do
      expect {
        subject.age
      }.to change(subject, :quality).by(3)
    end

  end

  context 'sell_in is zero or below' do
    let(:sell_in) { 0 }

    before { subject.age }

    specify { expect(subject.quality).to eq(0) }
  end

end
