require_relative '../spec_helper'

RSpec.describe GildedRose::PerishableItem do
  it_behaves_like 'item'

  subject { described_class.new(item) }

  let(:item)      { Item.new('name', sell_in, quality) }
  let(:sell_in)   { rand(2..10) }
  let(:quality)   { rand(4..50) }

  specify do
    expect {
      subject.age
    }.to change(subject, :quality).by(-1)
  end

  context 'if expired' do
    let(:sell_in)  { rand(-1..1) }

    specify do
      expect {
        subject.age
      }.to change(subject, :quality).by(-2)
    end
  end

  context 'if quality would go below zero' do

    let(:quality) { rand(-10..0) }

    before { subject.age }

    specify { expect(subject.quality).to eq(0) }

  end

end

