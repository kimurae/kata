require_relative '../spec_helper'

RSpec.describe GildedRose::AgedItem do
  it_behaves_like 'item'
  it_behaves_like 'appreciating_quality'

  subject { described_class.new(item) }

  let(:item) { Item.new('name', rand(10), rand(49)) }

  specify do
    expect {
      subject.age
    }.to change(subject, :quality).by(1)
  end

end

