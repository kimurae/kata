require_relative '../lib/gilded_rose'

RSpec.shared_examples 'item' do
  subject { described_class.new(item) }

  let(:item)    { Item.new('name', rand(42), quality) }
  let(:quality) { 1 }

  specify do
    expect {
      subject.age
    }.to change(subject, :sell_in).by(-1)
  end

  context 'quality is below 0' do
    let(:quality) { rand(-10..-1) }

    before { subject.age }

    specify { expect(subject.quality).to eq(0) }

  end

  context 'quality is above 50' do
    let(:quality) { rand(55..60) }

    before { subject.age }

    specify { expect(subject.quality).to eq(50) }

  end
end


RSpec.shared_examples 'appreciating_quality' do
  subject { described_class.new(item) }

  let(:item)    { Item.new('name', rand(-10..20), quality) }

  context 'if quality would go over 50' do
    let(:quality) { 50 }

    specify do
      expect {
        subject.age
      }.to change(subject, :quality).by(0)
    end
  end
end

