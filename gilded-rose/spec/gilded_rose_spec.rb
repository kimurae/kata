require_relative './spec_helper'

describe "#update_quality" do

  let(:aged_brie) {
    Item.new('Aged Brie', 3, 10)
  }

  let(:normal_item) {
    Item.new('NORMAL ITEM', 5, 10)
  }

  let(:items) {
    [ Item.new('Aged Brie', 3, 10),
      Item.new('Rubber Ducky', 4, 65),
      Item.new('Conjured Melon', 3, 22),
      Item.new('Conjured Angry Cat', 1, -20),
      Item.new('Backstage passes to Trogdor!', 8, 49),
      Item.new('Backstage passes to a TAFKAL80ETC concert', 10,10),
      Item.new('Sulfuras, Hand of Ragnaros', 0, 80)
    ]
  }

  subject { update_quality(items) }

  it 'should wrap these items in the right classes' do
    expect(subject.map(&:class)).to eq(
      [
        GildedRose::AgedItem,
        GildedRose::PerishableItem,
        GildedRose::ConjuredItem,
        GildedRose::ConjuredItem,
        GildedRose::LimitedItem,
        GildedRose::LimitedItem,
        GildedRose::LegendaryItem
      ]
    )
  end

end
