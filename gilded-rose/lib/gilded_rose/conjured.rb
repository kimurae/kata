module GildedRose

  module Conjured

    private

    def conjured?
      super || true
    end
  end

  private_constant :Conjured

end
