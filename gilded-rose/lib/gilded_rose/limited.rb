module GildedRose

  module Limited

    private

    def quality_for(current_quality, current_sell_in)
      raise QualityIsZeroInterrupt if current_sell_in < 0
      super + quality_appreciation(current_sell_in)
    end

    def quality_appreciation(current_sell_in)
      case sell_in
      when 1..5 then 2
      when 6..10 then 1
      else
          0
      end
    end

  end

  private_constant :Limited
end
