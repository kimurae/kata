module GildedRose

  # An item that appreciates over time.
  # until it reaches 50.
  class AgedItem < Item
    prepend Aged
  end

end
