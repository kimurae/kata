module GildedRose

  # @note When sell_in is greater than 10, quality increases by 1.
  # @note When sell_in is between 6 and 10, quality increases by 2.
  # @note When sell_in is between 1 and 5, quality increases by 3.
  # @note When sell_in is zero or less, quality is zero.
  class LimitedItem < Item
    prepend Aged
    prepend Limited
  end
end

