module GildedRose

  # A static item that never changes.
  class LegendaryItem < Item

    # Does nothing, quality and sell_in never change.
    def age; end
  end

end

