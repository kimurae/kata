module GildedRose

  # ConjuredItem is like Perishable item
  # except it depreciates twice as fast.
  class ConjuredItem < Item
    prepend Conjured
    prepend Perishable
  end

end
