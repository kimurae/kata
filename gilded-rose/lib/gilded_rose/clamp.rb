module GildedRose

  module Clamp
    refine Numeric do
      def clamp(range)
        [range.min, self, range.max].sort[1]
      end
    end
  end

  private_constant :Clamp

end
