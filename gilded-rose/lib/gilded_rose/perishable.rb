module GildedRose

  module Perishable

    private

    def conjured?
      super || false
    end

    def expired?(current_sell_in)
      sell_in <= 0
    end

    def quality_depreciation(current_sell_in)
      if conjured? && expired?(current_sell_in)
        4
      elsif conjured? || expired?(current_sell_in)
        2
      else
        1
      end
    end

    def quality_for(current_quality, current_sell_in)
      super - quality_depreciation(current_sell_in)
    end
  end

  private_constant :Perishable
end
