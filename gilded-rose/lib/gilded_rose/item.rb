module GildedRose

  # Wraps around Item
  class Item
    using Clamp
    extend Forwardable

    # @!attribute [r] name
    #   @return [String]

    # @!attribute [r] quality
    #   @return [Integer]

    # @!attribute [r] sell_in
    #   @return [Integer] the number of days until the item's expiration date.
    def_delegators :@item, :name, :quality, :sell_in

    # @param item [Struct] the original item this wraps around.
    def initialize(item)
      @item = item
    end

    # Deprecates sell_in by one.
    # Updates quality based on what modules are
    # prepended.
    # @note by default, quality just updates to itself.
    def age
      @item.sell_in -= 1
      @item.quality = begin
        quality_for(quality, sell_in).clamp 0..50
      rescue QualityIsZeroInterrupt
        0
      end
    end

    private

    def conjured?
      false
    end

    def quality_for(current_quality, current_sell_in)
      current_quality
    end
  end

end
