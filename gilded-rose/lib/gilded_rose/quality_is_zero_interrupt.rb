require 'forwardable'

module GildedRose

  class QualityIsZeroInterrupt < ::StandardError; end

  private_constant :QualityIsZeroInterrupt

end
