module GildedRose

  module Aged

    private

    def quality_for(current_quality, sell_in)
      super + 1
    end
  end

  private_constant :Aged

end
