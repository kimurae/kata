module GildedRose

  # An item deprecates over time.
  # @note quality drops by one until sell_in reaches zero.
  # @note quality drops by two when sell_in is below or at zero.
  class PerishableItem < Item
    prepend Perishable
  end

end

