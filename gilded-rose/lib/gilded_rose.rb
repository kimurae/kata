require 'forwardable'

require_relative './gilded_rose/clamp'
require_relative './gilded_rose/quality_is_zero_interrupt'

require_relative './gilded_rose/aged'
require_relative './gilded_rose/conjured'
require_relative './gilded_rose/limited'
require_relative './gilded_rose/perishable'

require_relative './gilded_rose/item'

require_relative './gilded_rose/aged_item'
require_relative './gilded_rose/conjured_item'
require_relative './gilded_rose/legendary_item'
require_relative './gilded_rose/limited_item'
require_relative './gilded_rose/perishable_item'

module GildedRose

end

def update_quality(items)
  items.map do |item|
    case item.name
    when 'Aged Brie' then GildedRose::AgedItem.new(item)
    when 'Sulfuras, Hand of Ragnaros' then GildedRose::LegendaryItem.new(item)
    when /^Backstage passes/ then GildedRose::LimitedItem.new(item)
    when /^Conjured/ then GildedRose::ConjuredItem.new(item)
    else
      GildedRose::PerishableItem.new(item)
    end.tap(&:age)
  end
end

######### DO NOT CHANGE BELOW #########

Item = Struct.new(:name, :sell_in, :quality)
